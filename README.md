# youtube-dl batch donwloader

Bash script to batch download youtube-dl comaptible URLs and merge the highest quality available audio .m4a and video .mp4 files into an exit .mp4 file placed into it's own directory. This can be useful for batch  downloading higher quality documentaries/movies from youtube or other [youtube-dl comaptible websites](https://ytdl-org.github.io/youtube-dl/supportedsites.html) assuming one has the right to do so.

 - upon initiating the script it will ask you to feed it a string of youtube-compatible URLs separated by a space
  - it will then download the audio .m4a and video .mp4 files with the highest available quality for each of the provided URLs
  - after each .m4a + .mp4 download combination completes, the 2 files are merged into an exit file using ffmped
  - the exit files is put in it's own directory named the title of the source video.
  - for the purpose of generating the exit file's and it's parent directory's names a /tmp/movie_tmp.txt file is generated which holds the output of the youtube-dl download process
  - after each merge has completed the audio *.m4a, video *.mp4 and /tmp/movie_tmp.txt files are all removed

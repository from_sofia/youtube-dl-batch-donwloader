#!/bin/bash

# allows to store strings with spaces in variables and then use mv/mkdir/rm on these
#IFS='
#'

# Promot for a URL
printf "************************************************************************************************************\n##  Please provide the youtube URL of the movie you want to download  ##\n************************************************************************************************************\n"
read -a list 

# Download and merge files for each URL provided
for i in "${list[@]}" ; do
    printf "************************************************************************************************************\n##  Please wait while the audio and video formats are being downloaded and the media has been merged..  ##\n##  Currently downloading: $i  ##\n************************************************************************************************************\n"
    
    #get the youtube-dl format codes for both the audio and video files 
    audio_code=$(youtube-dl -F $i | grep "m4a" | sort -k7n,7 | tail -1 | awk '{print $1}')
    video_code=$(youtube-dl -F $i | grep "video only" | grep "mp4" | sort -k3n,3 | tail -1 | awk '{print $1}')
    
    # download the audio and video formats and save the output to above created temp file
    movie_tmp=/tmp/movie_tmp.txt
    youtube-dl -f $audio_code,$video_code $i -o '%(title)s.%(ext)s' &> $movie_tmp
    
    # generate file names from above said temp file
    audio_file=$(grep "\[download\]\ Destination:" $movie_tmp | cut -c 25- | grep m4a)
    video_file=$(grep "\[download\]\ Destination:" $movie_tmp | cut -c 25- | grep mp4)
    movie_name=$(echo $audio_file | rev | cut -c 5- | rev)

    # allows to store strings with spaces in variables and then use mv/mkdir/rm on these
    IFS='
'
    # make the movie directory and merge audio + video into the movie
    mkdir $movie_name
    cd $movie_name
    ffmpeg -loglevel error -i "../$video_file" -i "../$audio_file" -strict -2 -codec copy "$movie_name.mp4"
    
    # clear no longer needed files
    rm $movie_tmp ../$audio_file ../$video_file
    
    printf "************************************************************************************************************\n##  The movie $movie_name has now been saved:  ##\n$(date ; hostname ; pwd ; du -sh *)\n************************************************************************************************************"
    
    # resturn to directory user was prior to initiating this script.
    cd ..
done

exit
